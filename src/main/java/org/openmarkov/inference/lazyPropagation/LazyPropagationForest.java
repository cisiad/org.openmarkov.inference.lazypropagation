/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.lazyPropagation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.openmarkov.core.action.CRemoveNodeEdit;
import org.openmarkov.core.action.PNESupport;
import org.openmarkov.core.action.RemoveLinkEdit;
import org.openmarkov.core.action.RemoveNodeEdit;
import org.openmarkov.core.exception.CanNotDoEditException;
import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NotEnoughMemoryException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.model.graph.Link;
import org.openmarkov.core.model.graph.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNode;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.inference.bayesianNetwork.huginPropagation.ClusterForest;
import org.openmarkov.inference.bayesianNetwork.huginPropagation.ClusterOfVariables;


/** This class implements an algorithm to create a 
 *  <code>openmarkov.inference.lazy.LazyPropagationForest</code> from a Markov
 *  network.
 * @author Carlos Baena
 * @author marias
 * @author fjdiez
 * @version 1.0
 * @since OpenMarkov 1.0
 * @see openmarkov.inference.ClusterForest
 * @see openmarkov.inference.lazy.LazyPropagationClique
 * @see openmarkov.inference.heuristics.EliminationHeuristic
 * @version 1.0 */
public class LazyPropagationForest extends ClusterForest {
	
	// Attributes
	/** It is a variable that exists during the construction of the 
	 * <code>LazyPropagationForest</code>. It is the collection of the cliques 
	 * waiting to be assigned a parent in the 
	 * <code>LazyPropagationForest</code>.
	 * Each clique comes with its separator to the variables remaining in the 
	 * <code>ProbNet</code>. It is implemented as a <code>HashMap</code> using
	 * the first separator variable as a key. */
	private HashMap<Variable, ArrayList<LazyPropagationClique>> orphanCliques;

	/** Used to build the <code>ClusterForest</code>. */
	private ProbNet markovNet;
	
	/** Log4j Logger */
	private Logger logger;
	
	// Constructors
	/** The <code>ProbNet</code> received will be destroyed
	 * @param markovNet <code>ProbNet</code> 
	 * @param heuristic <code>EliminationHeuristic</code> 
	 * @param logFile <code>LogFile</code>
	 * @param pNESupport <code>PNESupport</code> 
	 * @throws WrongCriterionException 
	 * @throws NonProjectablePotentialException 
	 * @throws DoEditException 
	 * @throws NotEnoughMemoryException */
	public LazyPropagationForest(ProbNet markovNet, 
			EliminationHeuristic heuristic, PNESupport pNESupport) 
	throws NotEnoughMemoryException, DoEditException, 
	NonProjectablePotentialException, WrongCriterionException {
		super();
		this.markovNet = markovNet;
		this.pNESupport = pNESupport;
        this.logger = Logger.getLogger (LazyPropagationForest.class);
		
		orphanCliques = 
			new HashMap<Variable, ArrayList<LazyPropagationClique>>();

		// eliminate variables
		Variable variableToDelete = heuristic.getVariableToDelete();

		// register the created heuristic as listener
		pNESupport.addUndoableEditListener(heuristic);

		while (variableToDelete != null) {
			deleteMarkovNetNode(variableToDelete, heuristic);
			variableToDelete = heuristic.getVariableToDelete();
		}
		
		// removes the heuristic from pNESupport
		pNESupport.removeUndoableEditListener(heuristic);	
		
	}

	/** The <code>ProbNet</code> received will be destroyed
	 * @argCondition <code>queryNodes</code> must have at least one node
	 * @param markovNet <code>ProbNet</code>
	 * @param heuristic <code>EliminationHeuristic</code>
	 * @param queryNodes <code>ArrayList</code> of <code>extends Node</code>
	 * @param logFile <code>LogFile</code>
	 * @param pNESupport <code>PNESupport</code> 
	 * @throws WrongCriterionException 
	 * @throws NonProjectablePotentialException 
	 * @throws CanNotDoEditException 
	 * @throws ConstraintViolationException 
	 * @throws NotEnoughMemoryException 
	 * @throws DoEditException */ 
	@SuppressWarnings("unchecked")
	public LazyPropagationForest(ProbNet markovNet, 
			EliminationHeuristic heuristic,
			ArrayList<Node> queryNodes, PNESupport pNESupport) 
	throws NotEnoughMemoryException, ConstraintViolationException, 
	CanNotDoEditException, NonProjectablePotentialException, 
	WrongCriterionException, DoEditException	{
		super();
		this.markovNet = markovNet;
		this.pNESupport = pNESupport;
		
		ArrayList<ProbNode> probQueryNodes = 
			(ArrayList<ProbNode>)((Object)queryNodes);
		markovNet.getGraph().marry(ProbNet.getNodesOfProbNodes(probQueryNodes));

		orphanCliques = 
			new HashMap<Variable, ArrayList<LazyPropagationClique>>();

		// eliminate variables
		Variable variableToDelete = heuristic.getVariableToDelete();
		
		while (variableToDelete != null) {
			CRemoveNodeEdit edit = new CRemoveNodeEdit(
					markovNet, variableToDelete);
			pNESupport.announceEdit(edit);
			deleteMarkovNetNode(variableToDelete, heuristic);
				pNESupport.doEdit(edit);
			variableToDelete = heuristic.getVariableToDelete();
		}
		
		// If the query variables form a clique (containing no other variables)
		// they will remain in the graph when all the non-query variables
		// have been eliminated
		if (markovNet.getNumNodes() > 0) {
			deleteMarkovNetNode(probQueryNodes.get(0).getVariable(), heuristic);
		} else {
			
		}
	}

	// Methods
	/** @see openmarkov.graphs.Graph#getLinks()
	 * @return <code>ArrayList</code> of <code>Link</code> */
	public ArrayList<Link> getLinks() {
		return null;  // because links are implicit
	}

	/** @return The set of cliques = rootCliques + its children (recursively) 
	 * + orphan cliques. <code>ArrayList</code> of <code>Node</code> */
	public ArrayList<ClusterOfVariables> getNodes() {
		// rootCliques + its children (recursively)
		ArrayList<ClusterOfVariables> nodes = super.getNodes();
	    // orphan cliques. recovers them in a HashMap because there are stored
      // in orphanCliques: a HashMap<Variable, ArrayList<LazyPropagationClique>>
		HashMap<LazyPropagationClique, LazyPropagationClique> auxOrphanCliques =
			new HashMap<LazyPropagationClique, LazyPropagationClique>();
		for (ArrayList<LazyPropagationClique> array : orphanCliques.values()) {
			for (LazyPropagationClique clique : array) {
				auxOrphanCliques.put(clique, clique);
			}
		}
		nodes.addAll(auxOrphanCliques.values());
		return nodes;
	}

	/** Change the <code>ClusterForest</code> structure to convert the given 
	 * <code>LazyPropagationClique</code> in <code>rootCluster</code> 
	 * @param newRoot <code>LazyPropagationClique</code> */
	public void convertToRoot(LazyPropagationClique newRoot) {
		Stack<LazyPropagationClique> cliquesStack = 
			new Stack<LazyPropagationClique>();
		LazyPropagationClique actualClique = 
			(LazyPropagationClique)rootClusters.get(0);
		
		// Store cliques from newRoot to the actual root in a Stack
		ArrayList<Node> parents = actualClique.getNode().getParents();
		while (parents != null) {
			actualClique = (LazyPropagationClique)parents.get(0).getObject();
			cliquesStack.add(actualClique);
			parents = actualClique.getNode().getParents();
		}
		
		// Create the new cliques and redirect links
		LazyPropagationClique root = cliquesStack.pop();
		LazyPropagationClique nextClique = cliquesStack.pop();
		while (nextClique != null) {
			// removes old link
			graph.removeLink(root.getNode(), nextClique.getNode(), true); 
			try {// creates the new link
				new Link(nextClique.getNode(), root.getNode(), true); 
			} catch (Exception e) {
				logger.fatal(e);
			} 
			root.setSeparatorVariables(nextClique.getSeparatorVariables());
			root = nextClique;
			nextClique = cliquesStack.pop();
		}
		
		// The root has no separator
		newRoot.setSeparatorVariables(new ArrayList<Variable>());
	}

	/** @param lazyPropagationClique <code>LazyPropagationClique</code>
	 * @param orphanCliques <code>HashMap</code> with key = 
	 * <code>Variable</code> and value = <code>ArrayList</code> of
	 * <code>LazyPropagationClique</code> */
	private void addOrphanClique(LazyPropagationClique lazyPropagationClique,
			HashMap<Variable, ArrayList<LazyPropagationClique>> orphanCliques) {
		// use the first separator variable as a key
		Variable key = lazyPropagationClique.getSeparatorVariables().get(0);
		
		ArrayList<LazyPropagationClique> cliqueList = orphanCliques.get(key);
		if (cliqueList == null) {
			cliqueList = new ArrayList<LazyPropagationClique>(); 
			orphanCliques.put(key, cliqueList);
		}
		cliqueList.add(lazyPropagationClique);
	}

	/** @param markovNet This <code>markovNet</code> will be destroyed
	 * @param heuristic <code>EliminationHeuristic</code>
	 * @param queryNodes <code>ArrayList</code> of <code>Node</code>
	 * @throws Exception */
	@SuppressWarnings("unused")
	private ClusterForest createForest(ProbNet markovNet, 
			EliminationHeuristic heuristic,
			ArrayList<Node> queryNodes) throws Exception{
		return new LazyPropagationForest(
				markovNet, heuristic,  queryNodes, pNESupport);
	}

	/** When adding a clique to the <code>LazyPropationForest</code> looks for
	 * the orphan cliques than can be connected as children of that clique.
	 * @param clique A <code>LazyPropagationClique</code> to be added to the 
	 * <code>LazyForest</code> 
	 * @param cliqueVariables <code>ArrayList</code> of <code>Variable</code>
	 * @param variablesToDelete <code>ArrayList</code> of <code>Variable</code>
	 * @param orphanCliques <code>HashMap</code> with key = 
	 * <code>Variable</code> and value = <code>ArrayList</code> of
	 * <code>LazyPropagationClique</code> */
	@SuppressWarnings("unchecked")
	private void relocateOrphanCliques(LazyPropagationClique clique,
			ArrayList<Variable> cliqueVariables,
			ArrayList<Variable> variablesToDelete,
			HashMap<Variable, ArrayList<LazyPropagationClique>> orphanCliques) {

		ArrayList<LazyPropagationClique> auxOrphanCliques;

		// Examines all the orphan cliques associated (in the hashMap) 
		// with some of the variables in the clique
		for (Variable variable : cliqueVariables) {
			auxOrphanCliques = orphanCliques.get(variable);
			if (auxOrphanCliques != null) {
				for (LazyPropagationClique orphanClique : 
						(ArrayList<LazyPropagationClique>)
						auxOrphanCliques.clone()) {
					// If all the separator variables in the orphan clique
					// are contained in the (new) current clique,
					// then the orphan clique turns into a
					// child of the current clique
					if (clique.containsAll(
							orphanClique.getSeparatorVariables())) {
						graph.addLink(
								clique.getNode(), orphanClique.getNode(), true);
						auxOrphanCliques.remove(orphanClique);
					}
				}
			}
		}
	}

	/** <code>variables2Cluster</code> associates a variable with a clique that
	 * is the root clique or that contains that variable in its separator.
	 * @param clique <code>LazyPropagationClique</code> */
	private void updateVariables2Clusters(LazyPropagationClique clique) {
		ArrayList<Variable> cliqueVariables = clique.getVariables();
		for (Variable variable : cliqueVariables) {
			LazyPropagationClique cliqueVariable = 
				(LazyPropagationClique)variables2Clusters.get(variable);
			if (cliqueVariable == null) {
				variables2Clusters.put(variable, clique);				
			} else {
				if ((cliqueVariable.separatorVariables != null) && 
					  (!cliqueVariable.separatorVariables.contains(variable)) &&
					  ((clique.separatorVariables == null) || 
					   (clique.separatorVariables.contains(variable)))) {
					variables2Clusters.put(variable, clique);									
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	/** @param variable <code>Variable</code>
	 * @param heuristic <code>EliminationHeuristic</code> */
	protected void deleteMarkovNetNode(Variable variable, 
			EliminationHeuristic heuristic) 
	throws NotEnoughMemoryException, DoEditException, 
	NonProjectablePotentialException, WrongCriterionException {
		ProbNode markovNetNode = markovNet.getProbNode(variable);
		ArrayList<ProbNode> neighborProbNodes = 
			ProbNet.getProbNodesOfNodes(markovNetNode.getNode().getNeighbors());
	
		// cliqueNodes = node + neighborNodes
		ArrayList<ProbNode> cliqueNodes = 
			(ArrayList<ProbNode>)neighborProbNodes.clone();
		cliqueNodes.add(markovNetNode);
		ArrayList<Variable> cliqueVariables = new ArrayList<Variable>();
		for (ProbNode cliqueNode : cliqueNodes) {
			cliqueVariables.add(cliqueNode.getVariable());
		}
		
		// separatorNodes = nodes having neighbors outside the clique
		ArrayList<ProbNode> separatorNodes = new ArrayList<ProbNode>();
		ArrayList<Variable> separatorVariables = new ArrayList<Variable>();
		
		// nodesToDelete = cliqueNodes that are not in the separator
		ArrayList<ProbNode> nodesToDelete = new ArrayList<ProbNode>();
		ArrayList<Variable> variablesToDelete = new ArrayList<Variable>();
		
		// classifies the nodes into separator nodes and nodes to be deleted
		for (ProbNode neighbor : cliqueNodes) { 
			if (graph.hasNeighborsOutside(neighbor.getNode(), 
					ProbNet.getNodesOfProbNodes(cliqueNodes))) {
				separatorNodes.add(neighbor);
				separatorVariables.add(neighbor.getVariable());
			} else {
				nodesToDelete.add(neighbor);
				variablesToDelete.add(neighbor.getVariable());
			}
		}
		
		LazyPropagationClique clique = new LazyPropagationClique(
				this, cliqueVariables, separatorVariables);
		updateVariables2Clusters(clique);
		
	
		// extracts the potentials whose variables are all in the clique
		// (examines first the separator nodes) and assigns those potentials 
		// to the clique
		ArrayList<Potential> potentialsToRemove;
		for (ProbNode separatorNode : separatorNodes) {
			potentialsToRemove = new ArrayList<Potential>();
			for (Potential potential : separatorNode.getPotentials()) {
				ArrayList<Variable> potentialVariables = potential
						.getVariables();
				if (clique.containsAll(potentialVariables)) {
					potentialsToRemove.add(potential);
					clique.addPriorPotential(potential);
				}
			}
			for (Potential potentialToRemove : potentialsToRemove) {
				separatorNode.removePotential(potentialToRemove);
			}
		}
	
		// extracts the potentials from the nodes to be deleted,
		// assigns those potentials to the clique and deletes those nodes
		HashMap<Node, Node> allSiblings =	new HashMap<Node, Node>(); 
		for (ProbNode nodeToDelete : nodesToDelete) {
			for (Potential potential : ((ProbNode)nodeToDelete).getPotentials())
			{
				clique.addPriorPotential(potential);
			}
			
			// Remove links
			ArrayList<Node> siblings = 
				(ArrayList<Node>)nodeToDelete.getNode().getSiblings().clone();
			for (Node sibling : siblings) {
				allSiblings.put(sibling, sibling);
				RemoveLinkEdit removeLinkEdit = 
					new RemoveLinkEdit(markovNet, 
						(Variable)nodeToDelete.getVariable(), 
						((ProbNode)sibling.getObject()).getVariable(), false);
					pNESupport.doEdit(removeLinkEdit);
			}
			
			// Remove node
			allSiblings.remove(nodeToDelete);
			RemoveNodeEdit removeNodeEdit = new 
				RemoveNodeEdit(markovNet, nodeToDelete.getVariable());
				pNESupport.doEdit(removeNodeEdit);
		}
		
		// marry siblings
		markovNet.getGraph().marry(ProbNet.getNodesOfProbNodes(separatorNodes));
		
		relocateOrphanCliques(clique, cliqueVariables, variablesToDelete, 
			orphanCliques);
	
		if (separatorVariables.isEmpty()) {
			setClusterAsRoot(clique);
		} else {
			addOrphanClique(clique, orphanCliques);
		}
	}

	/** @param potentialVariables <code>ArrayList</code> of 
	 * <code>Variable</code>
	 * @param separatorVariables <code>ArrayList</code> of 
	 * <code>Variable</code> 
	 * @return <code>boolean</code> */
	@SuppressWarnings("unused")
	private boolean intersectionNotEmpty(ArrayList<Variable> potentialVariables,
			ArrayList<Variable> separatorVariables) {
		for (Variable separatorVariable : separatorVariables) {
			for (Variable potentialVariable : potentialVariables) {
				if (separatorVariable == potentialVariable) {
					return true;
				}
			}
		}
		return false;
	}

}
