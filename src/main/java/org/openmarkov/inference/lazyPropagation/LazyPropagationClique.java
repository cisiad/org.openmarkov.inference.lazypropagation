/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.lazyPropagation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.VariableType;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.inference.huginPropagation.ClusterForest;
import org.openmarkov.inference.huginPropagation.ClusterOfVariables;


/** Contains a set of variables. The main difference with 
 *  <code>openmarkov.inference.ClusterOfVariables</code> is in methods 
 *  <code>openmarkov.inference.ClusterOfVariables#distributeEvidence</code> and 
 *  <code>openmarkov.inference.ClusterOfVariables#collectEvidence</code>. 
 *  @author Carlos Baena
 *  @author marias
 *  @author fjdiez */
public class LazyPropagationClique extends ClusterOfVariables {
	
	// Attributes
	protected static String clusterNamePrefix = "Clique.";

	protected int cliqueSize = 0;
	
	// The potential sets that represent the Messages in the Lazy 
	// Propagation scheme

	protected ArrayList<Potential> lazyUpgoingMessage = null;

	protected ArrayList<Potential> lazyDowngoingMessage = null;

	// Constructor
    /** @param cliqueVariables */
	public LazyPropagationClique(ClusterForest clusterForest, 
			ArrayList<Variable> cliqueVariables,
			ArrayList<Variable> separatorVariables) {
		super(clusterForest, cliqueVariables);
		name = clusterNamePrefix + (clusterForest.getNumNodes() - 1);
        this.separatorVariables = separatorVariables;
	}
	
	/** Calculates the marginalizated multiplicacion of: <code>priorPotentials, 
	 * evidencePotentials</code> and the recursively collected evidence from the 
	 * children of this <code>ClusterOfVariables</code>.
	 * The method in <code>ClusterofVariables</code> is overriden
	 * @param storageLevel If its value is 2 the collected evidence is 
	 * stored in the <code>posteriorPotential</code> without been marginalized.
	 * @return the result
	 * @throws NotEnoughMemoryException 
	 * @throws Exception if there is not enough memory */
	public Potential collectEvidence(int storageLevel) {
		
		if (upgoingMessage != null) { // It has been calculated before
			return upgoingMessage;
		}
		
		List<Potential> potentials= lazyCollectEvidence(storageLevel); 

		boolean isRootClique = separatorVariables.size() == 0;
		
		if (storageLevel == 2) {
			if (!isRootClique) {
				upgoingMessage = DiscretePotentialOperations.marginalize(
					posteriorPotential,	separatorVariables);
			} else {
				upgoingMessage = posteriorPotential;
			}
			return upgoingMessage;
		}
		
		if (storageLevel == 1) {
			if (!isRootClique) {
				upgoingMessage = 
						DiscretePotentialOperations.multiplyAndMarginalize(
								potentials, separatorVariables);
			} else {
				upgoingMessage = 
						DiscretePotentialOperations.multiply(potentials);
			}
			return upgoingMessage;
		}
		
		Potential collectedEvidence = null;
		if (!isRootClique) {
			collectedEvidence = 
					DiscretePotentialOperations.multiplyAndMarginalize(
							potentials, separatorVariables);
 		} else {
 			collectedEvidence = DiscretePotentialOperations.multiply(potentials);
 		}
		return collectedEvidence;

	}

	/** Returns a Lazy Propagation Message consisting in a Set of Potential.
	 * The potentials are held separated as long as possible
	 * They are multiplied only when necessary to perform a projection
	 * to a separator variable
	 * @param storageLevel If its value is 2 the collected evidence is 
	 * stored in the <code>posteriorPotential</code> without been marginalized.
	 * @return the result
	 * @throws NotEnoughMemoryException 
	 * @throws Exception if there is not enough memory */
	@SuppressWarnings("unchecked")
		private List<Potential> lazyCollectEvidence(int storageLevel) 
		{
			
			if (lazyUpgoingMessage != null) { // It has been calculated before
				return lazyUpgoingMessage;
			}
			
			collectEvidenceInvocations++;
			
			// adds the prior potentials and evidence potentials
			List<TablePotential> potentials = new ArrayList<>(priorPotentials);
			potentials.addAll(evidencePotentials);
			
			// recursively invokes collectEvidence on its children
			// and add the collected potentials
			List<ClusterOfVariables> children = (List<ClusterOfVariables>)((Object)getChildren());
			for (ClusterOfVariables child : children) {
				List<Potential> childPotentialSet =
					((LazyPropagationClique)child)
					.lazyCollectEvidence(storageLevel);
				potentials.addAll(childPotentialSet);
			}
			
	
			boolean isRootClique = separatorVariables.size() == 0;
			lazyUpgoingMessage=new ArrayList<Potential>();
			// Variable that hold the result of the potentials that need 
			// marginalization
			ArrayList<Potential> potentialsToMarginalize = 
					new ArrayList<Potential>(); 
            

			if (!isRootClique) {

				for (Potential singlePotential : potentials) {

					if (needsMarginalization(singlePotential, getVariables())) {
						potentialsToMarginalize.add(singlePotential);
					}
					else {
						lazyUpgoingMessage.add(singlePotential);
					}
				}

				lazyUpgoingMessage.add(DiscretePotentialOperations.
						multiplyAndMarginalize(potentialsToMarginalize,
								separatorVariables));
			}
			else {
				lazyUpgoingMessage.addAll(potentials);
			}

			if (storageLevel==2) {
				posteriorPotential = 
						DiscretePotentialOperations.multiply(potentials);
			}
			if (storageLevel==2 || storageLevel==1) {
				return lazyUpgoingMessage;
			}
			
			ArrayList<Potential> returnedPotentialSet = 
					(ArrayList<Potential>)lazyUpgoingMessage.clone();
			lazyUpgoingMessage=null;
			return returnedPotentialSet;
				
		}

	@SuppressWarnings("unchecked")
	/** @throws Exception */
	public void distributeEvidence(int storageLevel) 
	{
		distributeEvidenceInvocations++;

		boolean isRootClique = separatorVariables.size() == 0;
		
		List<LazyPropagationClique> children = (List<LazyPropagationClique>) ((Object) getChildren());
		HashMap<LazyPropagationClique,List<Potential>> lazyPotentialSets=
			new HashMap<LazyPropagationClique,List<Potential>>();

		// We populate an auxiliary variable that holds children upgoingMessage's
		for (LazyPropagationClique childClique : children) 
			lazyPotentialSets.put(
				childClique,childClique.getLazyUpgoingMessage(storageLevel));
		for (LazyPropagationClique childClique : children) {
			// adds the prior potentials and evidence potentials
			List<Potential> potentials = new ArrayList<Potential>(priorPotentials);
			potentials.addAll(evidencePotentials);
			HashMap<LazyPropagationClique,List<Potential>> 
				auxPotentialSets = new HashMap<>(lazyPotentialSets);
			// The potential that has come from the child clique is discarded
			auxPotentialSets.remove(childClique);
			// With the remaining potentials, including the downgoingMessage the
			// message to the child is calculated
	
			for (List<Potential> potentialSet : auxPotentialSets.values())
				potentials.addAll(potentialSet);
			if (!isRootClique)
				potentials.addAll(lazyDowngoingMessage);
			ArrayList<Potential> auxChildPotentialSet = 
					new ArrayList<Potential>();
			ArrayList<Potential> potentialsToMarginalize = 
					new ArrayList<Potential>();
			for (Potential singlePotential : potentials) {
				if (childClique.needsMarginalization(
						singlePotential,getVariables())) {
					potentialsToMarginalize.add(singlePotential);
				}
				else {
					auxChildPotentialSet.add(singlePotential);
				}
			}	
			auxChildPotentialSet.add(DiscretePotentialOperations.
					multiplyAndMarginalize(potentialsToMarginalize,
					        childClique.getSeparatorVariables()));
		     
			
			childClique.setLazyDowngoingPotential(auxChildPotentialSet);
			// Finally, the new PosteriorPotential is calculated and 
			// assigned to the child
			ArrayList<Potential> auxChildPotentials= new ArrayList<Potential>();
			auxChildPotentials.addAll(auxChildPotentialSet);
			Potential oldChildPosteriorPotential=childClique.
					getPosteriorPotential(storageLevel);
			auxChildPotentials.add(oldChildPosteriorPotential);	
			childClique.setPosteriorPotential(DiscretePotentialOperations.
					multiply(auxChildPotentials));

			childClique.distributeEvidence(storageLevel);
		}
	}

	/**
	 * @return true if all the variables in <code>variablesList</code> are
	 *         included in the clique variables
	 */
	public boolean containsAll(List<Variable> variablesList) {
		if (clusterVariables.containsAll(variablesList)) {
			return true;
		}
		return false;
	}

	/** @return lazyUpgoingMessage 
	 * @throws NotEnoughMemoryException 
	 * @throws Exception */
	public List<Potential> getLazyUpgoingMessage(int storageLevel) {
		if (lazyUpgoingMessage != null) {
			return lazyUpgoingMessage;
		}
		return lazyCollectEvidence(storageLevel);
	}

	/** @param potentialSet */
	protected void setLazyDowngoingPotential(
			ArrayList<Potential> potentialSet) {
		lazyDowngoingMessage = potentialSet;
	}

	/**
	 * @return true if the potential singlePotential needs
	 * marginalization
	 */
	@SuppressWarnings("unchecked")
	public boolean needsMarginalization(Potential singlePotential,
			List<Variable> cliqueVariables) {
		// We identify the relevant potentials for marginalization: 
		// that means: those containing variables that are not in the
		// separator or evidence variables
		// The remaining potentials are passed without transformation
		//We find which variables could make marginalization necessary 
		// First non separator Variables
		List<Variable> needToMarginalizeVariables = new ArrayList<>(cliqueVariables);
		needToMarginalizeVariables.removeAll(separatorVariables);
		// Then Evidence Variables
		for (Potential evidencePotential: evidencePotentials) {
			needToMarginalizeVariables.addAll(new ArrayList<> (evidencePotential.getVariables()));
		}	
		//If any of these variables is contained in the potential 
		// then this potential is added to potentialsToMarginalize
		List<Variable> singlePotentialVariables= new ArrayList<>(singlePotential.getVariables());
		singlePotentialVariables.retainAll(needToMarginalizeVariables);
		return !singlePotentialVariables.isEmpty();
	}
	
	/** @return Clique size */
	@SuppressWarnings("unchecked")
	public int size() {
		if (cliqueSize == 0) {
			cliqueSize = 1;
			ArrayList<Variable> cliqueVariables = 
				(ArrayList<Variable>)node.getObject();
			for (Variable variable : cliqueVariables) {
				if (variable.getVariableType() == VariableType.FINITE_STATES) {
					cliqueSize *= variable.getNumStates();
				}
			}
		}
		return cliqueSize;
	}

	
}
