/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.inference.lazyPropagation;


import java.util.ArrayList;

import org.openmarkov.core.action.PNESupport;
import org.openmarkov.core.exception.CanNotDoEditException;
import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.inference.heuristic.EliminationHeuristic;
import org.openmarkov.core.model.graph.Node;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.inference.huginPropagation.ClusterForest;
import org.openmarkov.inference.huginPropagation.ClusterPropagation;

/** Create objects for Lazy propagation algorithm inference. This class is 
 *  nearly empty because the algorithm is implemented in 
 *  <code>openmarkov.inference.lazy.LazyPropagationForest</code> construction 
 *  and in <code>openmarkov.inference.ClusterPropagation</code>.
 * @author Carlos Baena
 * @author marias
 * @author fjdiez
 * @version 1.0
 * @since OpenMarkov 1.0
 * @see openmarkov.graphs.Graph
 * @see openmarkov.inference.heuristics.CanoMoralElimination
 * @see openmarkov.networks.EvidenceCase
 * @see openmarkov.networks.Finding
 * @see openmarkov.inference.hugin
 * @see openmarkov.inference.hugin.HuginPropagation */
public class LazyPropagation extends ClusterPropagation {

    // Constructor
    /** @param probNet The graph to which the algorithm will be 
     * applied
	 * @param heuristic <code>EliminationHeuristic</code>
	 * @param pNESupport <code>PNESupport</code> 
     * @throws ConstraintViolationException 
     * @throws NotEvaluableNetworkException */
    public LazyPropagation(ProbNet probNet, EliminationHeuristic heuristic, 
    		PNESupport pNESupport) throws ConstraintViolationException,
    		NotEvaluableNetworkException {
    	super(probNet, heuristic, pNESupport);
    }

    // Methods
    @Override
    /** Creates a <code>LazyPropagationForest</code> from a Markov network
     *   having a <code>rootClique</code> containing all the 
     *   <code>queryNodes</code>. 
     * @return A <code>HuginForest</code> 
     * @param markovNet The Markov network will be destroyed. 
     *   (<code>ProbNet</code>)
     * @param heuristic <code>EliminationHeuristic</code>
     * @param queryNodes <code>ArrayList</code> of <code>Node</code> */
	protected ClusterForest createForest(ProbNet markovNet, 
			EliminationHeuristic heuristic,
			ArrayList<Node> queryNodes, PNESupport pNESupport) 
    throws NotEnoughMemoryException, ConstraintViolationException, 
    CanNotDoEditException, NonProjectablePotentialException, 
    WrongCriterionException, DoEditException {
		return new LazyPropagationForest(
				markovNet, heuristic, queryNodes, pNESupport);
	}
    
	@Override
    /** Creates a LazyPropagationForest for a ProbNet having a rootClique 
     * containing all the<code>queryNodes</code>. 
     * @return A <code>LazyPropagationForest</code> 
     * @param markovNet The <code>ProbNet</code> will be destroyed
     * @param heuristic <code>EliminationHeuristic</code>
     * @param pNESupport <code>PNESupport</code> */
	protected ClusterForest createForest(ProbNet markovNet, 
			EliminationHeuristic heuristic, PNESupport pNESupport) 
	throws NotEnoughMemoryException, DoEditException, 
	NonProjectablePotentialException, WrongCriterionException {
		return new LazyPropagationForest(markovNet, heuristic, pNESupport);
	}

    /** Creates a LazyForest for a ProbNet. 
     * @return A <code>LazyForest</code> 
     * @param markovNet The ProbNet will be destroyed 
     * @throws WrongCriterionException 
     * @throws NonProjectablePotentialException 
     * @throws DoEditException 
     * @throws NotEnoughMemoryException */
    protected ClusterForest createForest(
    		ProbNet markovNet, PNESupport pNESupport) 
    throws NotEnoughMemoryException, DoEditException, 
    NonProjectablePotentialException, WrongCriterionException {
    	return new LazyPropagationForest(markovNet, heuristic, pNESupport);
    }

}
